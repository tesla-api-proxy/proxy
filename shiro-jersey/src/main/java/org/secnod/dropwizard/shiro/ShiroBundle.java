package org.secnod.dropwizard.shiro;

import io.dropwizard.ConfiguredBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterRegistration;

import org.apache.shiro.realm.Realm;
import org.apache.shiro.web.env.IniWebEnvironment;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.mgt.WebSecurityManager;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.secnod.shiro.jersey.SubjectFactory;

/**
 * A Dropwizard bundle for Apache Shiro.
 */
public abstract class ShiroBundle<T> implements ConfiguredBundle<T> {

    @Override
    public void initialize(Bootstrap<?> bootstrap) {

    }

    @Override
    public void run(T configuration, Environment environment) {
        ShiroConfiguration shiroConfig = narrow(configuration);
        ResourceConfig resourceConfig = environment.jersey().getResourceConfig();

        resourceConfig.register(org.apache.shiro.web.jaxrs.ShiroAnnotationFilterFeature.class);
        resourceConfig.register(org.apache.shiro.web.jaxrs.SubjectPrincipalRequestFilter.class);
        resourceConfig.register(new SubjectFactory());

        Filter shiroFilter = createFilter(configuration);
        FilterRegistration.Dynamic registration = environment.servlets()
            .addFilter("ShiroFilter", shiroFilter);
        if (shiroConfig != null && shiroConfig.filterUrlPattern() != null) {
            registration.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, shiroConfig.filterUrlPattern());
        }
    }

    /**
     * Narrow down the complete configuration to just the Shiro configuration.
     */
    protected abstract ShiroConfiguration narrow(T configuration);


    /**
     * Create the Shiro filter. Overriding this method allows for complete customization of how Shiro is initialized.
     */
    protected Filter createFilter(final T configuration) {
        ShiroConfiguration shiroConfig = narrow(configuration);
        final IniWebEnvironment shiroEnv;
        if (shiroConfig != null && shiroConfig.iniConfigs() != null && shiroConfig.iniConfigs().length > 0) {
            shiroEnv = new IniWebEnvironment();
            shiroEnv.setConfigLocations(shiroConfig.iniConfigs());
            shiroEnv.init();
        } else {
            shiroEnv = null;
        }


        AbstractShiroFilter shiroFilter = new AbstractShiroFilter() {
            @Override
            public void init() throws Exception {
                setStaticSecurityManagerEnabled(true);
                Collection<Realm> realms = createRealms(configuration);
                WebSecurityManager securityManager;
                if (shiroEnv == null ){
                    securityManager = new DefaultWebSecurityManager(realms);
                } else if (realms.isEmpty() && shiroEnv != null) {
                    securityManager = shiroEnv.getWebSecurityManager();
                } else if (!realms.isEmpty()) {
                    securityManager = new DefaultWebSecurityManager(realms);
                } else {
                    securityManager = new DefaultWebSecurityManager();
                }
                setSecurityManager(securityManager);
                if (shiroEnv != null) {
                    setFilterChainResolver(shiroEnv.getFilterChainResolver());
                }
            }
        };
        return shiroFilter;
    }

    /**
     * Create and configure the Shiro realms. Override this method in order to
     * add realms that require configuration.
     *
     * @return a non-null list of realms. If empty, no realms will be added, but depending on the content of the INI
     *         file Shiro might still add its automatic IniRealm.
     */
    protected Collection<Realm> createRealms(T configuration) {
        return Collections.emptyList();
    }
}
