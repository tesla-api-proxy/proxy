package proxy.httpClient;

public class BadRequestException extends Exception {
    public BadRequestException(Exception ex) {
        super(ex);
    }
}
