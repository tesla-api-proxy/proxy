package proxy.httpClient;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.util.BackOff;
import com.google.api.client.util.ExponentialBackOff;

import proxy.auth.token.AccessToken;
import proxy.auth.token.tesla.TeslaAccessToken;

import java.io.IOException;
import java.util.Map;

public class TeslaHttpProxyClient {

    private final String metricName = "TeslaUpstream";
    private final MetricRegistry metricRegistry;
    private final HttpRequestFactory requestFactory;
    private final BackOff backOff = new ExponentialBackOff.Builder()
            .setInitialIntervalMillis(100)
        .setMaxElapsedTimeMillis(10000)
        .setMaxIntervalMillis(3000)
        .setMultiplier(1.5)
        .setRandomizationFactor(0.5)
        .build();

    private final String METER_100_RESPONSE;
    private final String METER_200_RESPONSE;
    private final String METER_300_RESPONSE;
    private final String METER_400_RESPONSE;
    private final String METER_500_RESPONSE;

    private final String COUNTER_EXECUTE;
    private final String TIMER_EXECUTE;

    private final String baseUrl = "https://owner-api.teslamotors.com";



    public TeslaHttpProxyClient(MetricRegistry registry) {
        this.metricRegistry = registry;
        this.METER_100_RESPONSE = MetricRegistry.name(metricName, "100");
        this.METER_200_RESPONSE = MetricRegistry.name(metricName, "200");
        this.METER_300_RESPONSE = MetricRegistry.name(metricName,"300");
        this.METER_400_RESPONSE = MetricRegistry.name(metricName, "400");
        this.METER_500_RESPONSE = MetricRegistry.name(metricName, "500");
        this.COUNTER_EXECUTE = MetricRegistry.name(metricName, ",execute");
        this.TIMER_EXECUTE = MetricRegistry.name(metricName,"time");

        HttpTransport transport = new NetHttpTransport.Builder().build();

        this.requestFactory = transport.createRequestFactory((request -> {
            request.setThrowExceptionOnExecuteError(false);
            request.setNumberOfRetries(2);
            request.setUnsuccessfulResponseHandler(
                    new HttpBackOffUnsuccessfulResponseHandler(backOff) {
                        @Override
                        public boolean handleResponse(HttpRequest request, HttpResponse response, boolean supportsRetry) throws IOException {
                            metricRegistry.meter(MetricRegistry.name(metricName, "response")).mark();
                            if (this.getBackOffRequired().isRequired(response)) {
                                metricRegistry.meter(MetricRegistry.name(metricName, "retry")).mark();
                            }
                            return super.handleResponse(request, response, supportsRetry);
                        }
                    }
            );
        }));
    }

    public HttpResponse executeRequest(String method, String relativeUrl, HttpContent content) throws BadUrlException, BadRequestException, IOException {
        return executeTeslaApiRequest(method, relativeUrl, content, null, null);
    }

    public HttpResponse executeTeslaApiRequest(String method, String relativeUrl, HttpContent content, Map<String,String> headers, TeslaAccessToken token) throws BadUrlException, BadRequestException, IOException {
        GenericUrl url;
        try {
            if (relativeUrl.startsWith("/")) {
                url = new GenericUrl(this.baseUrl + relativeUrl);
            } else {
                url = new GenericUrl(this.baseUrl + "/" + relativeUrl);
            }
        } catch (IllegalArgumentException ex) {
            throw new BadUrlException(ex);
        }
        HttpRequest request;
        try {
            request = this.requestFactory.buildRequest(method, url, content);
            if (headers != null) {
                headers.entrySet().forEach(header -> {
                    request.getHeaders().put(header.getKey(), header.getValue());
                });
            }
            if (token != null) {

                request.getHeaders().put("Authorization", String.format("Bearer %s", token.getToken()));
            }
        } catch (IOException ex) {
            throw new BadRequestException(ex);
        }
        final Counter executeCounter = metricRegistry.counter(COUNTER_EXECUTE);
        try (Timer.Context timer = metricRegistry.timer(TIMER_EXECUTE).time()) {
            executeCounter.inc();
            HttpResponse httpResponse = request.execute();
            if (httpResponse.getStatusCode() >= 100 && httpResponse.getStatusCode() <= 199) {
                metricRegistry.meter(METER_100_RESPONSE).mark();
            } else if (httpResponse.getStatusCode() >= 200 && httpResponse.getStatusCode() <= 299) {
                metricRegistry.meter(METER_200_RESPONSE).mark();
            } else if (httpResponse.getStatusCode() >= 300 && httpResponse.getStatusCode() <= 399) {
                metricRegistry.meter(METER_300_RESPONSE).mark();
            }
            if (httpResponse.getStatusCode() >= 400 && httpResponse.getStatusCode() <= 499) {
                metricRegistry.meter(METER_400_RESPONSE).mark();
            }
            if (httpResponse.getStatusCode() >= 500 && httpResponse.getStatusCode() <= 599) {
                metricRegistry.meter(METER_500_RESPONSE).mark();
            }
            return httpResponse;
        } finally {
            executeCounter.dec();
        }
    }
}
