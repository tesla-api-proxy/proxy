package proxy.httpClient;

public class BadUrlException extends Exception {
    public BadUrlException(Exception ex) {
        super(ex);
    }
}
