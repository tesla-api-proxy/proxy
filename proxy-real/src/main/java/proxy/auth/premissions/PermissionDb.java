package proxy.auth.premissions;

import org.apache.shiro.authz.Permission;
import proxy.auth.principal.ProxyServiceScopedPrincipal;


import java.util.Set;

public interface PermissionDb {
    Set<Permission> getAllowedPermissions(ProxyServiceScopedPrincipal principals);

    void setPermissions(ProxyServiceScopedPrincipal principal, Set<Permission> permissions);



}
