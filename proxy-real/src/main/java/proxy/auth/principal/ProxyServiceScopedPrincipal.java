package proxy.auth.principal;

import lombok.EqualsAndHashCode;
import lombok.Value;


@Value
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ProxyServiceScopedPrincipal {
    private final ProxyServiceAccountPrincipal mainAccount;
    private final String id;

    public ProxyServiceScopedPrincipal(ProxyServiceAccountPrincipal mainAccount, String id) {
        this.mainAccount = mainAccount;
        this.id = id;
    }

    @EqualsAndHashCode.Include
    public String getId() {
        return String.format("%s:%s", this.mainAccount.getId(), this.id);
    }
}