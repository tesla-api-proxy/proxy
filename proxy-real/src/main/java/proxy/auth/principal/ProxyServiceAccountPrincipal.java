package proxy.auth.principal;

import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode
public class ProxyServiceAccountPrincipal {
    private final String id;
}