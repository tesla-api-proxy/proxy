package proxy.auth.realm;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import proxy.auth.premissions.PermissionDb;
import proxy.auth.principal.ProxyServiceAccountPrincipal;
import proxy.auth.principal.ProxyServiceScopedPrincipal;
import proxy.auth.token.SimpleToken;
import proxy.auth.token.proxy.ProxyAccessToken;

@Slf4j
public class MockMainTokenRealm extends AuthorizingRealm  {

    private final PermissionDb permissionDb;

    public MockMainTokenRealm(PermissionDb permissionDb) {
        this.permissionDb = permissionDb;
        setAuthenticationTokenClass(BearerToken.class);
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        BearerToken bearerToken = (BearerToken)token;
        String actualStringToken = bearerToken.getToken();
        log.debug("Checking token " + actualStringToken);
        String key = "Bearer main:";
        if (actualStringToken.startsWith(key)) {
            ProxyServiceAccountPrincipal main = new ProxyServiceAccountPrincipal(actualStringToken.substring(actualStringToken.indexOf(key) + key.length()));

            SimplePrincipalCollection principalCollection = new SimplePrincipalCollection();
            principalCollection.add(main, this.getName());

            SimpleAuthenticationInfo auth = new SimpleAuthenticationInfo(principalCollection, bearerToken.getCredentials());
            return auth;
        }
        return new SimpleAuthenticationInfo();




    }
}
