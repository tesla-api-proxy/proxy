package proxy.auth.realm;

import java.util.Set;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.BearerToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

import lombok.extern.slf4j.Slf4j;
import proxy.auth.premissions.PermissionDb;
import proxy.auth.principal.ProxyServiceAccountPrincipal;
import proxy.auth.principal.ProxyServiceScopedPrincipal;
import proxy.auth.token.AccessToken;
import proxy.auth.token.SimpleToken;
import proxy.auth.token.db.TokenDb;
import proxy.auth.token.proxy.ProxyAccessToken;

@Slf4j
public class AccessTokenRealm extends AuthorizingRealm {

    private final PermissionDb permissionDb;
    private final TokenDb tokenDb;

    public AccessTokenRealm(PermissionDb permissionDb, TokenDb tokenDb) {
        setAuthenticationTokenClass(BearerToken.class);
        this.permissionDb = permissionDb;
        this.tokenDb = tokenDb;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        ProxyServiceScopedPrincipal scopedPrincipal = principals.oneByType(ProxyServiceScopedPrincipal.class);
        Set<Permission> allowedPermissions = permissionDb.getAllowedPermissions(scopedPrincipal);
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setObjectPermissions(allowedPermissions);
        return info;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        BearerToken bearerToken = (BearerToken)token;
        String actualStringToken = bearerToken.getToken();
        String key = "Bearer";
        if (actualStringToken.contains(key)) {
            actualStringToken = actualStringToken.substring(actualStringToken.indexOf(key) + key.length()).trim();
        }
        log.debug("Checking token " + actualStringToken);

        ProxyAccessToken accessToken = new SimpleToken(actualStringToken);
        ProxyServiceScopedPrincipal scopedPrincipal = tokenDb.getScopedPrincipalForAccessToken(accessToken);
        if (scopedPrincipal == null) {
            return new SimpleAuthenticationInfo();
        }

        SimplePrincipalCollection principalCollection = new SimplePrincipalCollection();
        principalCollection.add(scopedPrincipal, this.getName());
        SimpleAuthenticationInfo auth = new SimpleAuthenticationInfo(principalCollection, bearerToken.getCredentials());
        return auth;
    }
}