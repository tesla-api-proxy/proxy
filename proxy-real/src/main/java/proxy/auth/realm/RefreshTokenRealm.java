package proxy.auth.realm;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.apache.shiro.subject.SimplePrincipalCollection;
import proxy.auth.principal.ProxyServiceAccountPrincipal;
import proxy.auth.principal.ProxyServiceScopedPrincipal;
import proxy.auth.token.SimpleToken;
import proxy.auth.token.db.TokenDb;
import proxy.auth.token.proxy.ProxyRefreshToken;

@Slf4j
public class RefreshTokenRealm extends AuthenticatingRealm {

    private final TokenDb tokenDb;

    public RefreshTokenRealm(TokenDb tokenDb) {
        setAuthenticationTokenClass(RefreshAuthenticationToken.class);
        this.tokenDb = tokenDb;
    }

    @Value
    public static class RefreshAuthenticationToken implements AuthenticationToken {
        private final String token;

        @Override
        public Object getPrincipal() {
            return this.getToken();
        }

        @Override
        public Object getCredentials() {
            return this.getToken();
        }
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        RefreshAuthenticationToken refreshAuthenticationToken = (RefreshAuthenticationToken)token;
        String actualStringToken = refreshAuthenticationToken.getToken();
        log.debug("Checking token " + actualStringToken);

        ProxyRefreshToken refreshToken = new SimpleToken(actualStringToken);
        ProxyServiceScopedPrincipal scopedPrincipal = tokenDb.getScopedAccountForRefreshToken(refreshToken);
        if (scopedPrincipal == null) {
            throw new AuthenticationException("Invalid Refresh Token");
        }

        SimplePrincipalCollection principalCollection = new SimplePrincipalCollection();
        principalCollection.add(scopedPrincipal, this.getName());
        SimpleAuthenticationInfo auth = new SimpleAuthenticationInfo(principalCollection, refreshAuthenticationToken.getToken());
        return auth;
    }
}
