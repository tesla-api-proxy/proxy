package proxy.auth.token;

import proxy.auth.token.proxy.ProxyRefreshToken;

import java.time.Instant;

/**
 *
 */
public interface AccessToken extends Token {
    Instant getCreatedAt();
    long getExpiresInSeconds();
}
