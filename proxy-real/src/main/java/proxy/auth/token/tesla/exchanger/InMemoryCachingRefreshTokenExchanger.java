package proxy.auth.token.tesla.exchanger;

import java.util.concurrent.TimeUnit;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import proxy.auth.token.AccessToken;
import proxy.auth.token.proxy.ProxyRefreshToken;
import proxy.auth.token.tesla.TeslaAccessToken;
import proxy.auth.token.tesla.TeslaRefreshToken;

public class InMemoryCachingRefreshTokenExchanger implements RefreshTokenExchanger {

    private final RefreshTokenExchanger tokenExchanger;
    private final LoadingCache<TeslaRefreshToken, TeslaAccessToken> tokenCache;

    public InMemoryCachingRefreshTokenExchanger(RefreshTokenExchanger tokenExchanger) {
        this.tokenExchanger = tokenExchanger;
        this.tokenCache = Caffeine.newBuilder()
            .maximumSize(10_000)
            .expireAfterWrite(1, TimeUnit.DAYS)
            .refreshAfterWrite(1, TimeUnit.DAYS)
            .build(key -> this.tokenExchanger.exchangeRefreshTokenForAccessToken(key));
    }

    @Override
    public TeslaAccessToken exchangeRefreshTokenForAccessToken(TeslaRefreshToken proxyRefreshToken) {
        return this.tokenCache.get(proxyRefreshToken);
    }

    
}