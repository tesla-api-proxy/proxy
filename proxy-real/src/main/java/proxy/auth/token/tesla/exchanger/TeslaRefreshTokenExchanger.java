package proxy.auth.token.tesla.exchanger;

import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.InputStreamContent;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import proxy.auth.token.SimpleToken;
import proxy.auth.token.tesla.TeslaAccessToken;
import proxy.auth.token.tesla.TeslaRefreshToken;
import proxy.httpClient.BadRequestException;
import proxy.httpClient.BadUrlException;
import proxy.httpClient.TeslaHttpProxyClient;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.Instant;

public class TeslaRefreshTokenExchanger implements RefreshTokenExchanger {

    private final TeslaHttpProxyClient httpProxyClient;
    private final Gson gson;
    private final String clientId;
    private final String clientSecret;

    public TeslaRefreshTokenExchanger(TeslaHttpProxyClient httpProxyClient, String clientId, String clientSecret) {
        this.httpProxyClient = httpProxyClient;
        this.gson = new Gson();
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }


    @Override
    public TeslaAccessToken exchangeRefreshTokenForAccessToken(TeslaRefreshToken proxyRefreshToken) {
        JsonObject root = new JsonObject();
        root.addProperty("grant_type", "refresh_token");
        root.addProperty("client_id", this.clientId);
        root.addProperty("client_secret", this.clientSecret);
        root.addProperty("refresh_token", proxyRefreshToken.getToken());
        String json = gson.toJson(root);
        InputStreamContent content = new InputStreamContent("application/json", new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)));
        try {
            HttpResponse response = httpProxyClient.executeRequest("POST", "/oauth/token?grant_type=refresh_token", content);
            if (response.getStatusCode() != 200) {
                throw new RuntimeException("Could not exchange refresh token for access token");
            }
            JsonObject result = gson.fromJson(new InputStreamReader(response.getContent()), JsonObject.class);

            SimpleToken token = new SimpleToken(result.get("access_token").getAsString());
            long createdAt = result.get("created_at").getAsLong();
            long expiresIn = result.get("expires_in").getAsLong();
            Instant created = Instant.ofEpochMilli(createdAt);
            token.setCreatedAt(created);
            token.setExpiresAt(created.plusSeconds(expiresIn));
            return token;
        } catch (IOException | BadRequestException | BadUrlException ex) {
            // TODO: fix all the error cases
            throw new RuntimeException(ex);
        }
    }
}
