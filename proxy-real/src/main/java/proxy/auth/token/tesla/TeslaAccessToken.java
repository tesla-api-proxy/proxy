package proxy.auth.token.tesla;

import proxy.auth.token.AccessToken;

/**
 * Represents an {@link AccessToken} to call the tesla api with
 */
public interface TeslaAccessToken extends AccessToken {
}
