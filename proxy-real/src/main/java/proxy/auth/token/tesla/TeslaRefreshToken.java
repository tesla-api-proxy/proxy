package proxy.auth.token.tesla;

import proxy.auth.token.proxy.ProxyRefreshToken;

/**
 * Represents an upstream Tesla API token that can be exchanged for an AccessToken
 */
public interface TeslaRefreshToken extends ProxyRefreshToken {
}
