package proxy.auth.token.tesla.exchanger;

import proxy.auth.token.tesla.TeslaAccessToken;
import proxy.auth.token.tesla.TeslaRefreshToken;

/**
 * An interface for implementations that can exchange a {@link TeslaRefreshToken} into an {@link TeslaAccessToken}.
 */
public interface RefreshTokenExchanger {
    TeslaAccessToken exchangeRefreshTokenForAccessToken(TeslaRefreshToken proxyRefreshToken);
}
