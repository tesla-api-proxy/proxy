package proxy.auth.token.db;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.shiro.subject.Subject;
import proxy.auth.principal.ProxyServiceAccountPrincipal;
import proxy.auth.principal.ProxyServiceScopedPrincipal;
import proxy.auth.token.AccessToken;
import proxy.auth.token.proxy.ProxyAccessToken;
import proxy.auth.token.proxy.ProxyRefreshToken;
import proxy.auth.token.SimpleToken;
import proxy.auth.token.tesla.TeslaRefreshToken;

import java.security.GeneralSecurityException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class MemoryTokenDb implements TokenDb {

    private Map<ProxyServiceAccountPrincipal, TeslaRefreshToken> rootAccountsToRefreshTokens = new HashMap();
    private Map<ProxyServiceScopedPrincipal, ProxyServiceAccountPrincipal> scopedToMainAccounts = new HashMap<>();
    private Map<AccessToken, ProxyRefreshToken> accessTokenToProxyRefreshTokens = new HashMap<>();
    private Map<ProxyRefreshToken, ProxyServiceScopedPrincipal> refreshTokenToScopedPrincipal = new HashMap<>();
    private Map<ProxyRefreshToken, ProxyAccessToken> refreshTokenToAccessToken = new HashMap<>();


    @Override
    public TeslaRefreshToken getRefreshTokenForPrincipal(ProxyServiceAccountPrincipal account) {
        return rootAccountsToRefreshTokens.get(account);
    }

    @Override
    public void storeRefreshTokenForPrincipal(ProxyServiceAccountPrincipal account, TeslaRefreshToken token) throws GeneralSecurityException {
        this.rootAccountsToRefreshTokens.put(account, token);
    }

    @Override
    public ProxyServiceAccountPrincipal getProxyServiceAccountForScopedPrincipal(ProxyServiceScopedPrincipal scopedPrincipal) {
        return scopedToMainAccounts.get(scopedPrincipal);
    }

    @Override
    public ProxyServiceScopedPrincipal getScopedPrincipalForAccessToken(ProxyAccessToken token) {
        ProxyRefreshToken proxyRefreshToken = this.accessTokenToProxyRefreshTokens.get(token);
        return this.refreshTokenToScopedPrincipal.get(proxyRefreshToken);
    }

    @Override
    public void revokeAccessTokenForRefreshToken(Subject subject, ProxyRefreshToken proxyRefreshToken) {
        ProxyAccessToken accessTokenToRevoke = refreshTokenToAccessToken.remove(proxyRefreshToken);
        if (accessTokenToRevoke == null) {
            return;
        }
        this.accessTokenToProxyRefreshTokens.remove(accessTokenToRevoke);
    }

    @Override
    public ProxyAccessToken createAccessToken(Subject subject, ProxyRefreshToken proxyRefreshToken) {
        this.revokeAccessTokenForRefreshToken(subject, proxyRefreshToken);
        String at = RandomStringUtils.randomAlphanumeric(30);
        SimpleToken newAccessToken = new SimpleToken(at);
        newAccessToken.setCreatedAt(Instant.now());
        newAccessToken.setExpiresAt(newAccessToken.getCreatedAt().plus(45, ChronoUnit.DAYS));
        this.accessTokenToProxyRefreshTokens.put(newAccessToken, proxyRefreshToken);
        this.refreshTokenToAccessToken.put(proxyRefreshToken, newAccessToken);
        return newAccessToken;
    }

    @Override
    public ProxyServiceScopedPrincipal createSubAccount(ProxyServiceAccountPrincipal principal) {
        ProxyServiceScopedPrincipal newPrincipal = new ProxyServiceScopedPrincipal(principal, UUID.randomUUID().toString());
        this.scopedToMainAccounts.put(newPrincipal, principal);
        return newPrincipal;
    }

    @Override
    public ProxyRefreshToken createRefreshToken(ProxyServiceScopedPrincipal scopedPrincipal) {
        SimpleToken token = new SimpleToken(RandomStringUtils.randomAlphanumeric(30));
        this.refreshTokenToScopedPrincipal.put(token, scopedPrincipal);
        return token;
    }

    @Override
    public ProxyServiceScopedPrincipal getSubAccount(ProxyServiceAccountPrincipal root, String id) {
        ProxyServiceScopedPrincipal scopedById = new ProxyServiceScopedPrincipal(root, id);
        if (!this.scopedToMainAccounts.containsKey(scopedById)){
            return null;
        } else {
            return scopedById;
        }
    }

    @Override
    public ProxyServiceScopedPrincipal getScopedAccountForRefreshToken(ProxyRefreshToken token) {
        return this.refreshTokenToScopedPrincipal.get(token);
    }
}
