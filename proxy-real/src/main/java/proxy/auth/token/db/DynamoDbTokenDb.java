package proxy.auth.token.db;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.encryption.DynamoDBEncryptor;
import com.amazonaws.services.dynamodbv2.datamodeling.encryption.EncryptionContext;
import com.amazonaws.services.dynamodbv2.datamodeling.encryption.EncryptionFlags;
import com.amazonaws.services.dynamodbv2.datamodeling.encryption.providers.DirectKmsMaterialProvider;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.kms.AWSKMS;
import org.apache.shiro.subject.Subject;
import proxy.auth.principal.ProxyServiceAccountPrincipal;
import proxy.auth.principal.ProxyServiceScopedPrincipal;

import proxy.auth.token.proxy.ProxyAccessToken;
import proxy.auth.token.proxy.ProxyRefreshToken;
import proxy.auth.token.SimpleToken;
import proxy.auth.token.tesla.TeslaRefreshToken;

import java.security.GeneralSecurityException;
import java.util.*;

public class DynamoDbTokenDb implements TokenDb {

    private final AmazonDynamoDB ddb;
    private final DynamoDBEncryptor encryptor;
    private final EncryptionContext encryptionContext;

    private final String hashKeyName = "account_id";
    private final String refresh_token = "refresh_token";
    // private final String rangeKeyName = "b";
    private final String tableName;

    public DynamoDbTokenDb(AmazonDynamoDB ddb, AWSKMS kms, String tableName, String cmkArn) {
        this.ddb = ddb;
        this.tableName = tableName;
        final DirectKmsMaterialProvider cmp = new DirectKmsMaterialProvider(kms, cmkArn);
        this.encryptor = DynamoDBEncryptor.getInstance(cmp);

        this.encryptionContext = new EncryptionContext.Builder()
                .withTableName(tableName)
                .withHashKeyName(hashKeyName)
                // .withRangeKeyName(rangeKeyName)
                .build();


    }

    @Override
    public TeslaRefreshToken getRefreshTokenForPrincipal(ProxyServiceAccountPrincipal account) {
        GetItemResult item = this.ddb.getItem(new GetItemRequest()
                .withTableName(this.tableName)
                .withKey(Collections.singletonMap(hashKeyName, new AttributeValue(account.getId())))
        );
        Map<String, Set<EncryptionFlags>> actions = getEncryptionFlags(item.getItem());
        try {
            Map<String, AttributeValue> decryptedRecord = this.encryptor.decryptRecord(item.getItem(), actions,this.encryptionContext);
            return new SimpleToken(decryptedRecord.get(this.refresh_token).getS());
        } catch (GeneralSecurityException e) {
            throw new RuntimeException(e);
        }


    }

    public void storeRefreshTokenForPrincipal(ProxyServiceAccountPrincipal account, TeslaRefreshToken token) throws GeneralSecurityException {
        final Map<String, AttributeValue> record = new HashMap<>();
        record.put(this.hashKeyName, new AttributeValue().withS(account.getId()));
        // record.put(this.rangeKeyName, new AttributeValue().withN("55"));
//        record.put("example", new AttributeValue().withS("data"));
//        record.put("numbers", new AttributeValue().withN("99"));
//        record.put("binary", new AttributeValue().withB(ByteBuffer.wrap(new byte[]{0x00, 0x01, 0x02})));
//        record.put("test", new AttributeValue().withS("test-value"));
        record.put(refresh_token, new AttributeValue(token.getToken()));

        Map<String, Set<EncryptionFlags>> actions = getEncryptionFlags(record);

        final Map<String, AttributeValue> encrypted_record = encryptor.encryptRecord(record, actions, encryptionContext);

        this.ddb.putItem(this.tableName, encrypted_record);
    }

    @Override
    public ProxyServiceAccountPrincipal getProxyServiceAccountForScopedPrincipal(ProxyServiceScopedPrincipal scopedPrincipal) {
        return null;
    }

    @Override
    public ProxyServiceScopedPrincipal getScopedPrincipalForAccessToken(ProxyAccessToken token) {
        return null;
    }

    @Override
    public void revokeAccessTokenForRefreshToken(Subject subject, ProxyRefreshToken proxyRefreshToken) {

    }

    @Override
    public ProxyAccessToken createAccessToken(Subject subject, ProxyRefreshToken proxyRefreshToken) {
        return null;
    }

    @Override
    public ProxyServiceScopedPrincipal createSubAccount(ProxyServiceAccountPrincipal principal) {
        return null;
    }

    @Override
    public ProxyRefreshToken createRefreshToken(ProxyServiceScopedPrincipal scopedPrincipal) {
        return null;
    }

    @Override
    public ProxyServiceScopedPrincipal getSubAccount(ProxyServiceAccountPrincipal root, String id) {
        return null;
    }

    @Override
    public ProxyServiceScopedPrincipal getScopedAccountForRefreshToken(ProxyRefreshToken token) {
        return null;
    }

    private Map<String, Set<EncryptionFlags>> getEncryptionFlags(Map<String,AttributeValue> record) {
        final EnumSet<EncryptionFlags> signOnly = EnumSet.of(EncryptionFlags.SIGN);
        final EnumSet<EncryptionFlags> encryptAndSign = EnumSet.of(EncryptionFlags.ENCRYPT, EncryptionFlags.SIGN);


        final Map<String, Set<EncryptionFlags>> actions = new HashMap<>();

        for (final String attributeName : record.keySet()) {
            switch (attributeName) {
                case hashKeyName: // fall through to the next case
                // case rangeKeyName:
                    // Partition and sort keys must not be encrypted, but should be signed
                    actions.put(attributeName, signOnly);
                    break;
                case "test":
                    // Neither encrypted nor signed
                    break;
                default:
                    // Encrypt and sign all other attributes
                    actions.put(attributeName, encryptAndSign);
                    break;
            }
        }

        return actions;
    }
}
