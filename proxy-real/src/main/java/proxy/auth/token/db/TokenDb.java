package proxy.auth.token.db;

import org.apache.shiro.subject.Subject;
import proxy.auth.principal.ProxyServiceAccountPrincipal;
import proxy.auth.principal.ProxyServiceScopedPrincipal;
import proxy.auth.token.proxy.ProxyAccessToken;
import proxy.auth.token.proxy.ProxyRefreshToken;
import proxy.auth.token.tesla.TeslaRefreshToken;


import java.security.GeneralSecurityException;

public interface TokenDb {
    TeslaRefreshToken getRefreshTokenForPrincipal(ProxyServiceAccountPrincipal account);

    void storeRefreshTokenForPrincipal(ProxyServiceAccountPrincipal account, TeslaRefreshToken token) throws GeneralSecurityException;

    ProxyServiceAccountPrincipal getProxyServiceAccountForScopedPrincipal(ProxyServiceScopedPrincipal scopedPrincipal);

    ProxyServiceScopedPrincipal getScopedPrincipalForAccessToken(ProxyAccessToken token);

    void revokeAccessTokenForRefreshToken(Subject subject, ProxyRefreshToken proxyRefreshToken);

    ProxyAccessToken createAccessToken(Subject subject, ProxyRefreshToken proxyRefreshToken);

    ProxyServiceScopedPrincipal createSubAccount(ProxyServiceAccountPrincipal principal);

    ProxyRefreshToken createRefreshToken(ProxyServiceScopedPrincipal scopedPrincipal);

    ProxyServiceScopedPrincipal getSubAccount(ProxyServiceAccountPrincipal root, String id);

    ProxyServiceScopedPrincipal getScopedAccountForRefreshToken(ProxyRefreshToken token);

}
