package proxy.auth.token.proxy;

import proxy.auth.token.AccessToken;

public interface ProxyAccessToken extends AccessToken {
}
