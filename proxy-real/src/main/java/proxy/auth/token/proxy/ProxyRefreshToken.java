package proxy.auth.token.proxy;

import proxy.auth.token.AccessToken;
import proxy.auth.token.RefreshToken;

/**
 * A refresh token is typically a long lived token, that's used to obtain an {@link AccessToken}
 *
 * You obtain a RefreshToken by way of a username and password.
 */
public interface ProxyRefreshToken extends RefreshToken {

}
