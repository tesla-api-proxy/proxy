package proxy.auth.token;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import proxy.auth.token.proxy.ProxyAccessToken;
import proxy.auth.token.proxy.ProxyRefreshToken;
import proxy.auth.token.tesla.TeslaAccessToken;
import proxy.auth.token.tesla.TeslaRefreshToken;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
@AllArgsConstructor
public class SimpleToken implements ProxyAccessToken, ProxyRefreshToken, TeslaAccessToken, TeslaRefreshToken {
    @EqualsAndHashCode.Include
    private final String token;

    private Instant createdAt;
    private Instant expiresAt;

    public SimpleToken(String token) {
        this(token, null, null);
    }

    @Override
    public long getExpiresInSeconds() {
        return Instant.now().until(this.expiresAt, ChronoUnit.SECONDS);
    }

}