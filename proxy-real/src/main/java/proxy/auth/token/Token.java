package proxy.auth.token;

public interface Token {
    String getToken();
}
