package proxy.resources.subaccount;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.WildcardPermission;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.Subject;
import org.secnod.shiro.jaxrs.Auth;
import proxy.auth.premissions.PermissionDb;
import proxy.auth.principal.ProxyServiceAccountPrincipal;
import proxy.auth.principal.ProxyServiceScopedPrincipal;
import proxy.auth.token.db.TokenDb;
import proxy.auth.token.proxy.ProxyRefreshToken;
import proxy.resources.AbstractAuthResource;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.Collections;
import java.util.Set;

@Slf4j
@Path("/subAccount")
public class SubAccountResource extends AbstractAuthResource {

    private final TokenDb tokenDb;
    private final PermissionDb permissionDb;
    private final Realm mainAccountRealm;

    public SubAccountResource(TokenDb tokenDb, PermissionDb permissionDb, Realm mainAccountRealm) {
        this.tokenDb = tokenDb;
        this.permissionDb = permissionDb;
        this.mainAccountRealm = mainAccountRealm;
    }

    @POST
    public SubAccountResponse handleCreate(SubAccountRequest request, @Context HttpServletRequest httpRequest, @Auth Subject subject) {
        ProxyServiceAccountPrincipal rootAccountPrincipal = assertAndGetMainAccount(httpRequest, subject, this.mainAccountRealm);
        ProxyServiceScopedPrincipal scopedPrincipal = this.tokenDb.createSubAccount(rootAccountPrincipal);
        ProxyRefreshToken newRefreshToken = this.tokenDb.createRefreshToken(scopedPrincipal);
        Permission permission = new WildcardPermission("*");
        this.permissionDb.setPermissions(scopedPrincipal, Collections.singleton(permission));

        Set<Permission> allowedPermissions = this.permissionDb.getAllowedPermissions(scopedPrincipal);
        SubAccountResponse response = new SubAccountResponse();
        response.setId(scopedPrincipal.getId());
        // response.setPermissions(allowedPermissions);
        response.setRefreshToken(newRefreshToken.getToken());
        return response;
    }

//    @PUT
//    public SubAccountResponse handleUpdate(SubAccountRequest request,  @Context HttpServletRequest httpRequest, @Auth Subject subject) {
//        ProxyServiceAccountPrincipal rootAccountPrincipal = assertAndGetMainAccount(httpRequest, subject, this.mainAccountRealm);
//    }
//
//    @DELETE
//    public Response handleDelete(SubAccountRequest request,  @Context HttpServletRequest httpRequest, @Auth Subject subject) {
//        ProxyServiceAccountPrincipal rootAccountPrincipal = assertAndGetMainAccount(httpRequest, subject, this.mainAccountRealm);
//
//    }

    @GET
    @Path("/subAccount/{id}")
    public SubAccountResponse handleGet(@PathParam("id") String subAccountId, @Context HttpServletRequest httpRequest, @Auth Subject subject) {
        ProxyServiceAccountPrincipal rootAccountPrincipal = assertAndGetMainAccount(httpRequest, subject, this.mainAccountRealm);
        ProxyServiceScopedPrincipal scoped = tokenDb.getSubAccount(rootAccountPrincipal, subAccountId);
        if (scoped == null) {
            throw new NotFoundException();
        }
        SubAccountResponse response = new SubAccountResponse();
        response.setId(scoped.getId());
        return response;
    }
}
