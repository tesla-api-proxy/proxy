package proxy.resources.subaccount;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.shiro.authz.Permission;

import java.util.Set;

@Data
@NoArgsConstructor
public class SubAccountResponse {
    private String id;
    private String refreshToken;
    private Set<Permission> permissions;
}
