package proxy.resources.subaccount;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SubAccountRequest {
    private String id;
}
