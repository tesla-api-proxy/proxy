package proxy.resources.oauth;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.subject.Subject;
import org.secnod.shiro.jaxrs.Auth;
import proxy.auth.realm.RefreshTokenRealm;
import proxy.auth.token.AccessToken;
import proxy.auth.token.proxy.ProxyAccessToken;
import proxy.auth.token.proxy.ProxyRefreshToken;
import proxy.auth.token.SimpleToken;
import proxy.auth.token.db.TokenDb;
import proxy.resources.AbstractAuthResource;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

/**
 * This resource acts as the "oauth" endpoint for the proxy.
 *
 * Clients will be given a fake refresh token (also created by this service - see {@link proxy.resources.token.RefreshTokenResource})
 * which they use as if it were given by the tesla api.
 *
 * With that refresh token, clients will call this resource and we'll give them an access token.
 *
 */
@Slf4j
@Path("/oauth/token")
public class OauthResource {

    private final TokenDb tokenDb;

    public OauthResource(TokenDb tokenDb) {
        this.tokenDb = tokenDb;
    }

    private final String REFRESH_TOKEN = "refresh_token";
    private final String GRANT_TYPE = "grant_type";
    private final String BEARER_TYPE = "bearer";

    @POST
    public AccessTokenResponse handlePost(AccessTokenRequest request, @Context HttpServletRequest httpRequest, @Auth Subject subject) {
        String grantType = httpRequest.getParameter(GRANT_TYPE);
        if (!grantType.equals(REFRESH_TOKEN)) {
            throw new IllegalArgumentException();
        }
        String refreshToken = request.getRefreshToken();
        subject.login(new RefreshTokenRealm.RefreshAuthenticationToken(refreshToken));

        ProxyRefreshToken currentProxyRefreshToken = new SimpleToken(request.getRefreshToken());
        tokenDb.revokeAccessTokenForRefreshToken(subject, currentProxyRefreshToken);
        ProxyAccessToken newAccessToken = tokenDb.createAccessToken(subject, currentProxyRefreshToken);
        AccessTokenResponse response = new AccessTokenResponse();
        response.setAccessToken(newAccessToken.getToken());
        response.setCreatedAt(newAccessToken.getCreatedAt().toEpochMilli());
        response.setExpiresIn(newAccessToken.getExpiresInSeconds());
        response.setRefreshToken(currentProxyRefreshToken.getToken());
        response.setTokenType(BEARER_TYPE);
        return response;
    }
}
