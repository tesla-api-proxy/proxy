package proxy.resources.genericProxy;

import com.codahale.metrics.annotation.Timed;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.InputStreamContent;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.subject.Subject;
import org.secnod.shiro.jaxrs.Auth;

import proxy.audit.AuditedOperation;
import proxy.audit.AuditOperationFactory;
import proxy.auth.principal.ProxyServiceAccountPrincipal;
import proxy.auth.principal.ProxyServiceScopedPrincipal;
import proxy.auth.token.AccessToken;
import proxy.auth.token.proxy.ProxyRefreshToken;
import proxy.auth.token.tesla.TeslaAccessToken;
import proxy.auth.token.tesla.TeslaRefreshToken;
import proxy.auth.token.tesla.exchanger.RefreshTokenExchanger;
import proxy.auth.token.db.TokenDb;
import proxy.httpClient.BadRequestException;
import proxy.httpClient.BadUrlException;
import proxy.httpClient.TeslaHttpProxyClient;
import proxy.resources.AbstractAuthResource;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


/**
 * Handles all requests to the service, passing them along to the backend when
 * necessary
 */
@Path("/{subResources:.*}")
@Slf4j
public class GenericProxyResource extends AbstractAuthResource {

    private TeslaHttpProxyClient client;
    private TokenDb tokenDb;
    private RefreshTokenExchanger tokenExchanger;
    private AuditOperationFactory operationLogger;

    public GenericProxyResource(TeslaHttpProxyClient client, TokenDb tokenDb, RefreshTokenExchanger tokenExchanger, AuditOperationFactory auditFactory) {
        this.client = client;
        this.tokenDb = tokenDb;
        this.tokenExchanger = tokenExchanger;
        this.operationLogger = auditFactory;

    }

    @PATCH
    @Timed
    public Response handlePatch(Object request, @Context HttpServletRequest httpRequest, @Auth Subject subject) {
        return handle(httpRequest, subject);
    }

    @POST
    @Timed
    public Response handlePost(Object request, @Context HttpServletRequest httpRequest, @Auth Subject subject) {
        return handle(httpRequest, subject);

    }

    @GET
    @Timed
    public Response handleGet(Object request, @Context HttpServletRequest httpRequest, @Auth Subject subject) {
        return handle(httpRequest, subject);
    }

    @HEAD
    @Timed
    public Response handleHead(Object request, @Context HttpServletRequest httpRequest, @Auth Subject subject) {
        return handle(httpRequest, subject);
    }

    @PUT
    @Timed
    public Response handlePut(Object request, @Context HttpServletRequest httpRequest, @Auth Subject subject) {
        return handle(httpRequest, subject);
    }

    @OPTIONS
    @Timed
    public Response handleOptions(Object request, @Context HttpServletRequest httpRequest, @Auth Subject subject) {
        return handle(httpRequest, subject);
    }

    @DELETE
    @Timed
    public Response handleDelete(Object request, @Context HttpServletRequest httpRequest, @Auth Subject subject) {
        return handle(httpRequest, subject);
    }

    /**
     * The fun part!
     *
     * This method handles all requets not covered by existing resources.
     *
     * This method:
     * 1. Receives the incoming request from the caller,
     * 2. Discovers who the caller is - the caller should have provided an {@link AccessToken}
     * 3. Turns the requested call into a permission
     * 4. Validates the call is allowed (AuthZ)
     * 5. Log the operation
     * 6. Do throttle checking
     * 7. Execute the operation against the parent tesla api
     * 8. Return/Record results
     *
     * @param request
     * @param subject
     * @return
     */
    private Response handle(HttpServletRequest request, Subject subject) {
        doAuthZ(request, subject);
        ProxyServiceAccountPrincipal rootAccountPrincipal = this.tokenDb.getProxyServiceAccountForScopedPrincipal(subject.getPrincipals().oneByType(ProxyServiceScopedPrincipal.class));

        try (AuditedOperation operation = operationLogger.startOperation()) {
            operation
                    .withHttpRequest(request)
                    .withSubject(subject);
            TeslaRefreshToken teslaRefreshToken = tokenDb.getRefreshTokenForPrincipal(rootAccountPrincipal);
            TeslaAccessToken accessToken = tokenExchanger.exchangeRefreshTokenForAccessToken(teslaRefreshToken);

            Map<String, String> headers = new HashMap<>();
            Enumeration<String> headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String headerName = headerNames.nextElement();
                headers.put(headerName, request.getHeader(headerName));
            }
            try {
                InputStreamContent bodyContent = null;
                if (request.getContentLength() > 0) {
                    bodyContent = new InputStreamContent(request.getContentType(), request.getInputStream());
                }
                HttpResponse teslaResponse = client.executeTeslaApiRequest(request.getMethod(), request.getRequestURI(),
                        bodyContent, headers,
                        accessToken);
                return fromHttpResponse(teslaResponse);
            } catch (BadUrlException e) {
                // TODO Auto-generated catch block

                e.printStackTrace();
                return Response.status(Status.BAD_REQUEST).build();
            } catch (BadRequestException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return Response.status(Status.BAD_REQUEST).build();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return Response.serverError().build();
            }


        }




    }

    private Response fromHttpResponse(HttpResponse response) throws IOException {
        Response.ResponseBuilder builder = Response
            .status(response.getStatusCode())
            .type(response.getMediaType().toString())
            ;

        response.getHeaders().entrySet()
                .stream()
                .filter(header -> !header.getKey().equalsIgnoreCase("Content-Type"))
                .filter(header -> !header.getKey().equalsIgnoreCase("Content-Length"))
                .forEach(header -> {
                    builder.header(header.getKey(), header.getValue());
                });
        builder.entity(response.getContent());

        return builder.build();
    }




}
