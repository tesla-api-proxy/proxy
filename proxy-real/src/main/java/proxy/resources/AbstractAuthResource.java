package proxy.resources;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.BearerToken;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.Subject;
import proxy.auth.principal.ProxyServiceAccountPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import java.util.Collection;

@Slf4j
public abstract class AbstractAuthResource {

    public void doLogin(HttpServletRequest request, Subject subject) {
        subject.logout();
        subject.login(new BearerToken(request.getHeader("Authorization")));
    }

    public void doAuthZ(HttpServletRequest request, Subject subject) {
        doLogin(request, subject);
        String permission = String.format("%s:%s", request.getMethod(), request.getPathInfo());
        doAuthZ(permission, subject);
    }

    public void doAuthZ(String permission, Subject subject) {
        log.debug("Checking if {} can perform {}", subject.getPrincipal(), permission);
        if (!subject.isPermitted(permission)) {
            log.debug("{} is NOT allowed to perform {}", subject.getPrincipal().toString(), permission);
            throw new ForbiddenException(String.format("Subject %s is now allowed to invoke %s",
                    subject.getPrincipal() == null ? "anonymous" : subject.getPrincipal().toString(), permission));
        }
        log.debug("{} is allowed to perform {}", subject.getPrincipal().toString(), permission);
    }

    public ProxyServiceAccountPrincipal assertAndGetMainAccount(HttpServletRequest request, Subject subject, Realm mainAccountRealm) {
        doLogin(request, subject);
        Collection mainAccountPrincipals = subject.getPrincipals().fromRealm(mainAccountRealm.getName());
        if (mainAccountPrincipals.size() != 1) {
            throw new ForbiddenException("Only owning account principals are allowed to call this method");
        }
        return (ProxyServiceAccountPrincipal)mainAccountPrincipals.stream().findFirst().get();
    }

}
