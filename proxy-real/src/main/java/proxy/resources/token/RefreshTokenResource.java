package proxy.resources.token;

import com.codahale.metrics.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.Subject;
import org.secnod.shiro.jaxrs.Auth;
import proxy.auth.principal.ProxyServiceAccountPrincipal;
import proxy.auth.token.SimpleToken;
import proxy.auth.token.db.TokenDb;
import proxy.resources.AbstractAuthResource;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.security.GeneralSecurityException;
import java.util.Collection;

@Path("/refreshToken")
@Slf4j
public class RefreshTokenResource extends AbstractAuthResource {

    private final TokenDb tokenDb;
    private final Realm mainAccountRealm;

    public RefreshTokenResource(TokenDb tokenDb, Realm mainAccountRealm) {
        this.tokenDb = tokenDb;
        this.mainAccountRealm = mainAccountRealm;
    }

    @POST
    @Timed
    public Response handlePost(RefreshTokenRequest request, @Context HttpServletRequest httpRequest, @Auth Subject subject) {
        ProxyServiceAccountPrincipal rootAccountPrincipal = assertAndGetMainAccount(httpRequest, subject, this.mainAccountRealm);
        try {
            this.tokenDb.storeRefreshTokenForPrincipal(rootAccountPrincipal, new SimpleToken(request.getToken()));
            return Response.ok().build();
        } catch (GeneralSecurityException e) {
            throw new InternalServerErrorException();
        }
    }
}
