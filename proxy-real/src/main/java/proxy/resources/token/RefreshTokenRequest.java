package proxy.resources.token;

import lombok.NoArgsConstructor;
import lombok.Value;

@Value
@NoArgsConstructor
public class RefreshTokenRequest {
    private String token = null;
}
