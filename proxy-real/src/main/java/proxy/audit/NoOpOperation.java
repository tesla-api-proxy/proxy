package proxy.audit;

import org.apache.shiro.subject.Subject;

import javax.servlet.http.HttpServletRequest;

public class NoOpOperation implements AuditedOperation {
    @Override
    public AuditedOperation withSubject(Subject subject) {
        return this;
    }

    @Override
    public AuditedOperation withHttpRequest(HttpServletRequest request) {
        return this;
    }

    @Override
    public AuditLogger getLogger() {
        return new NoOpAuditLogger();
    }

    @Override
    public void close() {

    }
}
