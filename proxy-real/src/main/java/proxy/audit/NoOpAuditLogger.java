package proxy.audit;

public class NoOpAuditLogger implements AuditLogger {
    @Override
    public void logDebug(String logLine) {

    }

    @Override
    public void logInfo(String logLine) {

    }

    @Override
    public void logWarn(String logLine) {

    }

    @Override
    public void logDebug(String format, Object... args) {

    }

    @Override
    public void logInfo(String format, Object... args) {

    }

    @Override
    public void logWarn(String format, Object... args) {

    }
}
