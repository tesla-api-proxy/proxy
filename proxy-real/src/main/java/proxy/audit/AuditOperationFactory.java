package proxy.audit;

public interface AuditOperationFactory {
    public AuditedOperation startOperation();

}
