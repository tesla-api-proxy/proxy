package proxy.audit;

import org.apache.shiro.subject.Subject;

import javax.servlet.http.HttpServletRequest;

public interface AuditedOperation extends AutoCloseable {
    AuditedOperation withSubject(Subject subject);
    AuditedOperation withHttpRequest(HttpServletRequest request);
    AuditLogger getLogger();
    // AuditedOperation withOutcome(AuditedOperationResult result);
    void close(); // explicitly don't throw
}
