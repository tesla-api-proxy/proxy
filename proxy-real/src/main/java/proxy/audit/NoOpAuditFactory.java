package proxy.audit;

public class NoOpAuditFactory implements AuditOperationFactory {
    @Override
    public AuditedOperation startOperation() {
        return new NoOpOperation();
    }
}
