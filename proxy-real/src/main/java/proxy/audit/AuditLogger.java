package proxy.audit;

public interface AuditLogger {
    void logDebug(String logLine);
    void logInfo(String logLine);
    void logWarn(String logLine);

    void logDebug(String format, Object... args);
    void logInfo(String format, Object... args);
    void logWarn(String format, Object... args);
}
