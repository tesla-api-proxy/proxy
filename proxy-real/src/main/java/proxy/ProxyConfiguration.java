package proxy;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import org.secnod.dropwizard.shiro.ShiroConfiguration;

import io.dropwizard.Configuration;

public class ProxyConfiguration extends Configuration {
    public static enum ProxyStage {
        PROD("prod"),
        DEVO("devo"),
        LOCAL("local");

        private String name;

        private ProxyStage(String name) {
            this.name = name;
        }

        @JsonCreator
        public static ProxyStage forValue(String value) {
            return ProxyStage.valueOf(value);
        }

        @JsonValue
        public String toValue() {
            return this.name;
        }
    }

    @JsonProperty
    ShiroConfiguration shiro;
    
    @JsonProperty()
    ProxyStage stage;

    @JsonProperty
    String clientId;

    @JsonProperty
    String clientSecret;
}
