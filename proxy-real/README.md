# Tesla Proxy

This is a thin "proxy" layer on top of the Tesla Car API


While it's excellent that tesla provides an API for interacting with your car (albiet in an unoffical way) - using that API in other apps can be a bit scary. You effectively have to give those applications either your Tesla Account Credentials or an equally scary refresh token.

To further complicate things, a poorly behaving app with access to those credentials can un-necessarily cause vampire drain on your car - which is also not ideal.
And further still, it's an all or nothing access level, sometimes, I want an app that just shows my car state - but doesn't need to be able to unlock it!

The goal of this package is to solve these woes, but remain API compatible with the existing tesla API. Ideally, unoffical clients could simply point to a version of this and be good to go.

## Security

As mentioned, the scary parts of trusting a third party with your car account is just that - you have to trust a third party with access to your car. There's a wealth of information and potentially dangerous actions those third parties could take without your knowledge.
So why should you trust this? Well, you shouldn't honestly - I'm also a third party (aka, random stranger on the internet) who also could do scary things.

A few goals with this project to make it less scary:

1. Fully open source code and insight into how it's deployed
2. Can be self hosted by you and the code can be audited to ensure we're not randomly sending credentials anywhere
3. Reports (Audits) on usage of apis by callers (applications)
4. Encryption at rest of refresh tokens

## Goals

Ideally, this proxy solves the problem of:

1. Not having to give your root tesla credentials out
2. Centralized "wake" throttling to avoid vampire drain
3. Scoping of permissions per application
4. Auditing of calls

## FAQ

### How is this different from TeslaScope?

TeslaScope is honestly amazing - we're not trying to compete with them.
In an ideal world, TeslaScope would use this (TeslaProxy) for making calls for some users.
Perhaps you don't want to give them access to everything, or you want to know when and how they're accessing your data - that's the goal.


## Building it

```
./gradlew shadowJar
```


## Running it

```
java -jar $PROJECT_ROOT/build/libs/proxy-all.jar server $PROJECT_ROOT/src/main/resources/<config to run with>
```
